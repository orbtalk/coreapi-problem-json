from coreapi import codecs


class ProblemJSON(codecs.JSONCodec):
    media_type = 'application/problem+json'
